package statesource

import (
	"errors"
)

// SelectStateSource returns the proper Source implementation depending on the
// backend type specified the the configuration
func SelectStateSource(backendConfig *BackendConfig) (Source, error) {
	switch backendConfig.Backend {
	case "artifactory":
		return newArtifactorySource(backendConfig.URL, backendConfig.Repository, backendConfig.Username, backendConfig.Password)
	case "s3":
		return newS3Source(backendConfig.URL, backendConfig.Bucket, backendConfig.AccessKey, backendConfig.SecretKey)
	case "terraform-cloud":
		return newTfCloudSource(backendConfig.URL, backendConfig.Organization, backendConfig.Token)
	}

	return nil, errors.New("Unsupported backend: " + backendConfig.Backend)
}
