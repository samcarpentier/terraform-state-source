package statesource

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

const artifactoryBaseURL = "http://artifactory.example.com"
const repository = "repo"
const username = "username"
const password = "s3cr3t"
const artifactoryMockTfState = `{
	"version": 3,
	"terraform_version": "0.11.10",
	"modules": [
		{
			"resources": {
				"openstack_compute_instance_v2.myinstance.0": {
					"type": "openstack_compute_instance_v2",
					"primary": {
						"attributes": {
							"name": "fqdn.example.com"
						}
					}
				},
				"otherresource": {
					"type": "otherresource",
					"primary": {
						"attributes": {
							"name": "fqdn2.example.com"
						}
					}
				}
			}
		}
	]
}`

func TestSuccessfullyGetRemoteStateArtifactory(t *testing.T) {
	testServer := setupTestServerArtifactory()
	defer testServer.Close()
	artifactorySource := getArtifactorySource(testServer.URL)

	tfStateFileContent, _ := artifactorySource.GetRemoteState("path/in/repo")

	assert.Equal(t, artifactoryMockTfState, tfStateFileContent)
}

func TestNoBaseURLReturnsErrorArtifactory(t *testing.T) {
	s, err := newArtifactorySource("", repository, username, password)

	assert.Nil(t, s)
	if assert.NotNil(t, err) {
		assert.Equal(t, "No baseURL provided", err.Error())
	}
}

func TestNoRepositoryReturnsErrorArtifactory(t *testing.T) {
	s, err := newArtifactorySource(artifactoryBaseURL, "", username, password)

	assert.Nil(t, s)
	if assert.NotNil(t, err) {
		assert.Equal(t, "No repository provided", err.Error())
	}
}

func TestNoUsernameReturnsErrorArtifactory(t *testing.T) {
	s, err := newArtifactorySource(artifactoryBaseURL, repository, "", password)

	assert.Nil(t, s)
	if assert.NotNil(t, err) {
		assert.Equal(t, "No username provided", err.Error())
	}
}

func TestNoPasswordReturnsErrorArtifactory(t *testing.T) {
	s, err := newArtifactorySource(artifactoryBaseURL, repository, username, "")

	assert.Nil(t, s)
	if assert.NotNil(t, err) {
		assert.Equal(t, "No password provided", err.Error())
	}
}

func TestAllParametersValidReturnsNoErrorArtifactory(t *testing.T) {
	s, err := newArtifactorySource(artifactoryBaseURL, repository, username, password)

	assert.Nil(t, err)
	assert.IsType(t, &artifactorySource{}, s)
}

func setupTestServerArtifactory() *httptest.Server {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, artifactoryMockTfState)
	}))

	return ts
}

func getArtifactorySource(baseURL string) *artifactorySource {
	return &artifactorySource{
		baseURL:    baseURL,
		repository: repository,
		username:   username,
		password:   password,
	}
}
