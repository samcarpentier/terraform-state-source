package statesource

// Source is an interface that allows to fetch a remote TF state file
type Source interface {
	GetRemoteState(string) (string, error)
}
