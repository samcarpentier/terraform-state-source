package statesource

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSelectBackendArtifactory(t *testing.T) {
	backendConfig := getBackendConfig("artifactory")

	selectedSource, err := SelectStateSource(backendConfig)

	assert.Nil(t, err)
	if assert.NotNil(t, selectedSource) {
		assert.IsType(t, &artifactorySource{}, selectedSource)
	}
}

func TestSelectBackendS3(t *testing.T) {
	backendConfig := getBackendConfig("s3")

	selectedSource, err := SelectStateSource(backendConfig)

	assert.Nil(t, err)
	if assert.NotNil(t, selectedSource) {
		assert.IsType(t, &s3Source{}, selectedSource)
	}
}

func TestSelectBackendTfCloud(t *testing.T) {
	backendConfig := getBackendConfig("terraform-cloud")

	selectedSource, err := SelectStateSource(backendConfig)

	assert.Nil(t, err)
	if assert.NotNil(t, selectedSource) {
		assert.IsType(t, &tfCloudSource{}, selectedSource)
	}
}

func TestSelectBackendUnknownReturnsError(t *testing.T) {
	backendConfig := getBackendConfig("unknown")

	selectedSource, err := SelectStateSource(backendConfig)

	assert.Nil(t, selectedSource)
	if assert.NotNil(t, err) {
		assert.Equal(t, "Unsupported backend: unknown", err.Error())
	}
}

func getBackendConfig(backendType string) *BackendConfig {
	return &BackendConfig{
		Backend:      backendType,
		URL:          "http://www.test.com",
		Paths:        []string{"abc/", "def/"},
		Repository:   "repo",
		Username:     "myself",
		Password:     "pass123",
		Bucket:       "bucket",
		AccessKey:    "access",
		SecretKey:    "secret123",
		Organization: "MyOrg",
		Token:        "token123",
	}
}
