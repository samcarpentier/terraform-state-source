package statesource

import (
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

type artifactorySource struct {
	baseURL    string
	repository string
	username   string
	password   string
}

func newArtifactorySource(baseURL string, repository string, username string, password string) (*artifactorySource, error) {
	if len(baseURL) == 0 {
		return nil, errors.New("No baseURL provided")
	}
	if len(repository) == 0 {
		return nil, errors.New("No repository provided")
	}
	if len(username) == 0 {
		return nil, errors.New("No username provided")
	}
	if len(password) == 0 {
		return nil, errors.New("No password provided")
	}

	return &artifactorySource{
		baseURL:    baseURL,
		repository: repository,
		username:   username,
		password:   password,
	}, nil
}

func (a *artifactorySource) GetRemoteState(path string) (string, error) {
	tfStateFilePath, _ := url.Parse(strings.TrimRight(a.baseURL, "/") + "/" + a.repository + "/" + path + "/terraform.tfstate")

	client := &http.Client{}
	req, err := http.NewRequest("GET", tfStateFilePath.String(), nil)
	req.SetBasicAuth(a.username, a.password)

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("Artifactory communication error: %s", err)
		return "", err
	}
	if resp.StatusCode != 200 {
		log.Fatalf("Unable to fetch tfstate file from Artifactory: %s", resp.Status)
		return "", errors.New(resp.Status)
	}

	bodyText, err := ioutil.ReadAll(resp.Body)
	return strings.TrimSpace(string(bodyText)), nil
}
