package statesource

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

const tfCloudBaseURL = "https://app.terraform.io"
const organization = "myorg"
const token = "token123"
const tfCloudMockTfState = `{
	"version": 3,
	"terraform_version": "0.11.10",
	"modules": [
		{
			"resources": {
				"openstack_compute_instance_v2.myinstance.0": {
					"type": "openstack_compute_instance_v2",
					"primary": {
						"attributes": {
							"name": "fqdn.example.com"
						}
					}
				},
				"otherresource": {
					"type": "otherresource",
					"primary": {
						"attributes": {
							"name": "fqdn2.example.com"
						}
					}
				}
			}
		}
	]
}`

func TestSuccessfullyGetRemoteStateTfCloud(t *testing.T) {
	testServer := setupTestServerTfCloud()
	defer testServer.Close()
	tfCloudSource := getTfCloudSource(testServer.URL)

	tfStateFileContent, _ := tfCloudSource.GetRemoteState("my-workspace")

	assert.Equal(t, tfCloudMockTfState, tfStateFileContent)
}

func TestNoBaseURLReturnsErrorTfCloud(t *testing.T) {
	s, err := newTfCloudSource("", organization, token)

	assert.Nil(t, s)
	if assert.NotNil(t, err) {
		assert.Equal(t, "No baseURL provided", err.Error())
	}
}

func TestNoOrganizationReturnsErrorTfCloud(t *testing.T) {
	s, err := newTfCloudSource(tfCloudBaseURL, "", token)

	assert.Nil(t, s)
	if assert.NotNil(t, err) {
		assert.Equal(t, "No organization provided", err.Error())
	}
}

func TestNoTokenReturnsErrorTfCloud(t *testing.T) {
	s, err := newTfCloudSource(tfCloudBaseURL, organization, "")

	assert.Nil(t, s)
	if assert.NotNil(t, err) {
		assert.Equal(t, "No token provided", err.Error())
	}
}

func TestAllParametersValidReturnsNoErrorTfCloud(t *testing.T) {
	s, err := newTfCloudSource(tfCloudBaseURL, organization, token)

	assert.Nil(t, err)
	assert.IsType(t, &tfCloudSource{}, s)
}

func setupTestServerTfCloud() *httptest.Server {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.URL.String(), "/api/v2/state-versions") {
			fmt.Fprintf(w, `{ "data": [{ "attributes": { "hosted-state-download-url": "http://`+r.Host+`/mockedstate.tfstate" } }] }`)
		} else {
			fmt.Fprintf(w, tfCloudMockTfState)
		}
	}))

	return ts
}

func getTfCloudSource(baseURL string) *tfCloudSource {
	return &tfCloudSource{
		baseURL:      baseURL,
		organization: organization,
		token:        token,
	}
}
