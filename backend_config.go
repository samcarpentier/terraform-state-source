package statesource

// Backend represents the access information for a Terraform remote state backend
type BackendConfig struct {
	Backend string   `yaml:"backend"`
	URL     string   `yaml:"url"`
	Paths   []string `yaml:"paths"`

	// Artifactory
	Repository string `yaml:"repository"`
	Username   string `yaml:"username"`
	Password   string `yaml:"password"`

	// S3
	Bucket    string `yaml:"bucket"`
	AccessKey string `yaml:"access_key"`
	SecretKey string `yaml:"secret_key"`

	// Terraform Cloud
	Organization string `yaml:"organization"`
	Token        string `yaml:"token"`
}
