package statesource

import (
	"crypto/tls"
	"errors"
	"fmt"

	"net/http"

	minio "github.com/minio/minio-go"
)

type s3Source struct {
	endpointURL string
	bucket      string
	accessKey   string
	secretKey   string
}

// New instantiates a new s3Source
func newS3Source(endpointURL string, bucket string, accessKey string, secretKey string) (*s3Source, error) {
	if len(endpointURL) == 0 {
		return nil, errors.New("No endpointURL provided")
	}
	if len(bucket) == 0 {
		return nil, errors.New("No bucket provided")
	}
	if len(accessKey) == 0 {
		return nil, errors.New("No accessKey provided")
	}
	if len(secretKey) == 0 {
		return nil, errors.New("No secretKey provided")
	}

	return &s3Source{
		endpointURL: endpointURL,
		bucket:      bucket,
		accessKey:   accessKey,
		secretKey:   secretKey,
	}, nil
}

func (s *s3Source) GetRemoteState(path string) (string, error) {
	useSSL := true
	minioClient, err := minio.New(s.endpointURL, s.accessKey, s.secretKey, useSSL)
	// Disable proxy and certificate verification
	minioClient.SetCustomTransport(&http.Transport{
		Proxy: nil,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	})
	if err != nil {
		return "", fmt.Errorf("Unable to connect to S3 API: %s", err)
	}

	objectPath := path + "/terraform.tfstate"
	objectReader, err := minioClient.GetObject(s.bucket, objectPath, minio.GetObjectOptions{})
	if err != nil {
		return "", fmt.Errorf("Unable to retrieve TFState object in S3 bucket: %s", err)
	}

	objectStat, err := objectReader.Stat()
	if err != nil {
		return "", fmt.Errorf("Unable to get S3 object size: %s", err)
	}

	buffer := make([]byte, objectStat.Size)
	_, err = objectReader.Read(buffer)
	if err != nil {
		if err.Error() != "EOF" {
			return "", fmt.Errorf("Unable to read S3 object content: %s", err)
		}
	}

	return string(buffer), nil
}
