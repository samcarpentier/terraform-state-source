# terraform-state-source

Terraform State Source is a Go library that can be used to acquire Terraform remote state files from multiple sources in a transparent manner.

## Supported Terraform Backends

* Artifactory
* S3
* Terraform Cloud

## Usage

```go
import (
  statesource "gitlab.com/samcarpentier/terraform-state-source"
)
```

## License

Apache License 2.0
