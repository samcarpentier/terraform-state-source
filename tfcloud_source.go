package statesource

import (
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/tidwall/gjson"
)

type tfCloudSource struct {
	baseURL      string
	organization string
	token        string
}

type tfCloudStateDescriptor struct {
	attributes map[string]interface{}
}

type tfCloudWorkspaceStatesDescriptor struct {
	data []tfCloudStateDescriptor
}

func newTfCloudSource(baseURL string, organization string, token string) (*tfCloudSource, error) {
	if len(baseURL) == 0 {
		return nil, errors.New("No baseURL provided")
	}
	if len(organization) == 0 {
		return nil, errors.New("No organization provided")
	}
	if len(token) == 0 {
		return nil, errors.New("No token provided")
	}

	return &tfCloudSource{
		baseURL:      baseURL,
		organization: organization,
		token:        token,
	}, nil
}

func (t *tfCloudSource) GetRemoteState(workspace string) (string, error) {
	latestStateDownloadURL, err := t.getLatestStateDownloadURL(workspace)
	if err != nil {
		log.Fatalf("Unable to get latest state's download URL from %s: %s", t.baseURL, err)
		return "", err
	}

	client := &http.Client{}
	req, err := http.NewRequest("GET", latestStateDownloadURL, nil)
	req.Header.Add("Authorization", "Bearer "+t.token)
	req.Header.Add("Content-Type", "application/vnd.api+json")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("Terraform Cloud communication error: %s", err)
		return "", err
	}
	if resp.StatusCode != 200 {
		log.Fatalf("Unable to fetch tfstate file from %s: %s", t.baseURL, resp.Status)
		return "", errors.New(resp.Status)
	}

	bodyText, err := ioutil.ReadAll(resp.Body)
	return strings.TrimSpace(string(bodyText)), nil
}

func (t *tfCloudSource) getLatestStateDownloadURL(workspace string) (string, error) {
	tfStateDescriptorBasePath, _ := url.Parse(strings.TrimRight(t.baseURL, "/") + "/api/v2/state-versions")
	query := url.Values{}
	query.Add("filter[organization][name]", t.organization)
	query.Add("filter[workspace][name]", workspace)
	query.Add("page[size]", "1")
	tfStateDescriptorBasePath.RawQuery = query.Encode()

	client := &http.Client{}
	req, err := http.NewRequest("GET", tfStateDescriptorBasePath.String(), nil)
	req.Header.Add("Authorization", "Bearer "+t.token)
	req.Header.Add("Content-Type", "application/vnd.api+json")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("Terraform Cloud communication error: %s", err)
		return "", err
	}
	if resp.StatusCode != 200 {
		log.Fatalf("Unable to fetch workspace's states descriptor from %s: %s", t.baseURL, resp.Status)
		return "", errors.New(resp.Status)
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	bodyString := string(bodyBytes)
	stateDownloadURL := gjson.Get(bodyString, "data.0.attributes.hosted-state-download-url")

	return stateDownloadURL.String(), nil
}
